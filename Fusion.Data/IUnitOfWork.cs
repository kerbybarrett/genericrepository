﻿using System;
using System.Data;

namespace Fusion.Data
{
    /// <summary>
    /// Contract for performing the Unit Of Work pattern
    /// </summary>
   public interface IUnitOfWork : IDisposable
    {
       /// <summary>
       /// Disposes of the resources associated with the class
       /// </summary>
       /// <param name="disposing"></param>
       void Dispose(bool disposing);

        /// <summary>
        /// Gets whether the operation is inside of a transaction.
        /// </summary>
        /// <value></value>
        bool IsInTransaction { get; }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        int SaveChanges();

        /// <summary>
        /// Begins the transaction.
        /// </summary>
        /// <param name="isolationLevel">The isolation level.</param>
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);

        /// <summary>
        /// Rolls back a transaction.
        /// </summary>
        void RollBack();

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        /// <returns></returns>
        bool Commit();
    }
}
