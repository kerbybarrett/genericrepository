﻿namespace Fusion.Data
{
    /// <summary>
    /// Order for repository sorts
    /// </summary>
    public enum SortOrder
    {

        /// <summary>
        /// Sorts elements in ascending order
        /// </summary>
        Ascending,

        /// <summary>
        /// Sorts elements in descending order
        /// </summary>
        Descending
    }

}
