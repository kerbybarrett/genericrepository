﻿using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

namespace Fusion.Data
{
    public static class RepositoryExtensions
    {
        /// <summary>
        /// Gets a single entity by predicate function
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static TEntity GetSingleOrDefault<TEntity>(this IRepository repository, Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            Contract.Requires<ArgumentNullException>(repository != null);
            Contract.Requires<ArgumentNullException>(predicate != null);

            return repository.GetQuery(predicate).SingleOrDefault();
        }

        /// <summary>
        /// Gets a single entity by predicate function
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="repository"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static TEntity GetFirstOrDefault<TEntity>(this IRepository repository, Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            Contract.Requires<ArgumentNullException>(repository != null);
            Contract.Requires<ArgumentNullException>(predicate != null);

            return repository.GetQuery(predicate).FirstOrDefault();
        }
    }
}
