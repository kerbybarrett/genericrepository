﻿using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Fusion.Collections.Generic;
using Fusion.Data;

namespace System.Collections.Generic
{
    public static class QueryableExtensions
    {
        /// <summary>
        /// Converts an IQueryable resultset to a single page of results based on paging metadata
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <typeparam name="TKey">The type of the key.</typeparam>
        /// <param name="query">The query.</param>
        /// <param name="pageIndex">Index of the page (1-based).</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="keySelector">The key selector.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <returns></returns>
        public static PagedList<TEntity> ToPagedList<TEntity, TKey>(this IQueryable<TEntity> query, int pageIndex, int pageSize, Expression<Func<TEntity, TKey>> keySelector, SortOrder sortOrder = SortOrder.Ascending)
        {
            Contract.Requires<ArgumentNullException>(query != null);
            Contract.Requires<ArgumentNullException>(keySelector != null);
            Contract.Requires<ArgumentException>(pageIndex >= 0);
            Contract.Requires<ArgumentException>(pageSize >= 0);

            int totalCount = query.Count();

            var ordered = sortOrder == SortOrder.Ascending
                                ? query.OrderBy(keySelector)
                                : query.OrderByDescending(keySelector);

            IQueryable<TEntity> collection = ordered.Skip((pageIndex - 1) * pageSize).Take(pageSize);

            return new PagedList<TEntity>(collection, pageIndex, pageSize, totalCount);
        }
       
        /// <summary>
        /// Converts a property expression and equality value to an IQueryable predicate expression
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="property">The property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static Expression<Func<TEntity, bool>> PropertySelectorToFilterPredicate<TEntity, TProperty>(
            this Expression<Func<TEntity, TProperty>> property, TProperty value)
            where TProperty : IComparable
        {
            Contract.Requires<ArgumentNullException>(property != null);

            var memberExpression = property.Body as MemberExpression;
            if (memberExpression == null || !(memberExpression.Member is PropertyInfo)) {

                throw new ArgumentException("Property expression expected", "property");
            }

            Expression left = property.Body;
            Expression right = Expression.Constant(value, typeof(TProperty));
            Expression searchExpression = Expression.Equal(left, right);
            return Expression.Lambda<Func<TEntity, bool>>(
                searchExpression, new[] { property.Parameters.Single() });
        }
    }
}
