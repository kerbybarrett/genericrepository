﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Fusion.Data
{
    /// <summary>
    /// Interface for interacting with an entity repository
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Add a new entity to the repository
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        void Add<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Add a new entity and related entities to the repository
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        void AddGraph<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Update an existing entity in the repository
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        void Update<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Delete an entity in the repository
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        void Delete<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Returns an <see cref="IQueryable" /> query for the given entity type
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns></returns>
        IQueryable<TEntity> GetQuery<TEntity>() where TEntity : class;

        /// <summary>
        /// Returns an <see cref="IQueryable" /> query for the given entity type meeting a specific predicate
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        IQueryable<TEntity> GetQuery<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns>The number of records that were affected</returns>
        int SaveChanges();

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>The unit of work.</value>
        IUnitOfWork UnitOfWork { get; }
    }
}
