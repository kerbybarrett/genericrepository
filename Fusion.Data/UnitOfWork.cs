﻿using System.Data;

namespace Fusion.Data
{
    /// <summary>
    /// Default <see cref="UnitOfWork"/> implementation that does nothing on its own.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private bool isInTransaction = false;

        /// <summary>
        /// Gets whether the operation is inside of a transaction.
        /// </summary>
        public bool IsInTransaction
        {
            get { return isInTransaction; }
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns></returns>
        public int SaveChanges()
        {
            return 0;
        }

        /// <summary>
        /// Begins the transaction.
        /// </summary>
        /// <param name="isolationLevel">The isolation level.</param>
        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            isInTransaction = true;
        }

        /// <summary>
        /// Rolls back a transaction.
        /// </summary>
        public void RollBack()
        {
            isInTransaction = false;
        }

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        public bool Commit()
        {
            isInTransaction = false;
            return true;
        }

        /// <summary>
        /// Disposes of the resources associated with the class
        /// </summary>
        /// <param name="disposing"></param>
        public void Dispose(bool disposing)
        {
            // No-op
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // No-op
        }
    }
}
