﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Fusion.Data.EntityFramework.Tests
{
    [TestClass]
    public class RepositoryTests
    {
        static RepositoryTests()
        {
            // HACK: Reference the CE assembly so that the assembly is deployed
            var _ = System.Data.Entity.SqlServerCompact.SqlCeProviderServices.Instance;
        }

        [TestMethod]
        public void RawContext_ShouldGetData()
        {
            using (var context = new ModelContext())
            {
                var person = context.People.FirstOrDefault();
                Assert.IsNotNull(person);
            }
        }

        [TestMethod]
        public void Repository_ShouldGetData()
        {
            using (var repository = new ModelRepository())
            {
                var person = repository.GetQuery<Person>().FirstOrDefault();
                Assert.IsNotNull(person);
            }
        }

        [TestMethod]
        public void Repository_ShouldUpdateData()
        {
            using (var repository = new ModelRepository())
            {
                var person = new Person { Name = "Alpha" };
                repository.Add(person);
                repository.SaveChanges();

                person = repository.GetFirstOrDefault<Person>(p => p.Name.StartsWith("Alpha"));
                person.Name = "Beta";
                repository.Update(person);
                repository.SaveChanges();

                person = repository.GetFirstOrDefault<Person>(p => p.Id == person.Id);
                Assert.AreEqual("Beta", person.Name);
            }
        }

        [TestMethod]
        public void Repository_ShouldInsertData()
        {
            using (var repository = new ModelRepository())
            {
                var person = new Person { Name = "Omega" };
                repository.Add<Person>(person);
                repository.SaveChanges();

                person = repository.GetFirstOrDefault<Person>(p => p.Name.StartsWith("Omega"));
                Assert.IsNotNull(person);
            }
        }

        [TestMethod]
        public void Repository_ShouldInsertGraphData()
        {
            using (var repository = new ModelRepository())
            {
                var person = new Person { Name = "Tango" };
                var car = new Car { Model = "Tango Car" };
                person.Cars.Add(car);

                repository.AddGraph(person);
                repository.SaveChanges();

                var selected = repository.GetFirstOrDefault<Car>(c => c.Model == "Tango Car");
                Assert.IsNotNull(selected);
            }
        }

        [TestMethod]
        public void Repository_ShouldGetGraphWhenRequested()
        {
            using (var repository = new ModelRepository())
            {
                var people = repository.GetQueryWithGraph<Person>(p => p.Cars).ToList();

                Assert.IsTrue(people.Count > 0);
                Assert.IsTrue(people[0].Cars.Count > 0);
            }
        }

        [TestMethod]
        public void Repository_PaginateWorksCorrectly()
        {
            using (var repository = new ModelRepository())
            {
                var paged = repository.GetQueryWithGraph<Person>(person => person.Cars).ToPagedList(1, 10, p => p.Id);

                Assert.AreEqual(10, paged.PageSize);
                Assert.AreEqual(1, paged.PageIndex);
                Assert.IsTrue(paged.Count > 0);
            }
        }

        [TestMethod]
        public void Repository_ShouldDeleteData()
        {
            using (var repository = new ModelRepository())
            {
                var person = new Person { Name = "Gamma" };
                repository.Add<Person>(person);
                repository.SaveChanges();

                var inserted = repository.GetFirstOrDefault<Person>(p => p.Id == person.Id);
                repository.Delete(inserted);
                repository.SaveChanges();

                person = repository.GetFirstOrDefault<Person>(p => p.Name.StartsWith("Gamma"));
                Assert.IsNull(person);
            }
        }

        [TestMethod]
        public void Repository_ShouldSupportObjectGraphIncludes()
        {
            using (var repository = new ModelRepository())
            {
                var person = repository.GetQuery<Person>().Include(p => p.Cars).First();
                Assert.IsNotNull(person.Cars);
                Assert.IsTrue(person.Cars.Count > 0);
            }
        }

        [TestMethod]
        public void UnitOfWork_ShouldSupportDisposable_Correctly()
        {
            int count;
            using (var repository = new ModelRepository())
            {
                count = repository.GetQuery<Person>().Count();

                repository.UnitOfWork.BeginTransaction();
               
                var person = new Person { Name = "Mike" };
                repository.Add<Person>(person);

                // We are not completing the transaction; we expect the test to roll back;
            }

            using (var repository = new ModelRepository())
            {
                var people = repository.GetQuery<Person>();
                Assert.AreEqual<int>(count, people.Count());
            }
        }
    }
}
