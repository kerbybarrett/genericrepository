﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Fusion.Data.EntityFramework.Tests
{
    internal class CarConfiguration : EntityTypeConfiguration<Car>
    {
        public CarConfiguration()
        {
            this.ToTable("Car");
            this.HasKey(c => c.Id);
            this.Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(c => c.Model).HasColumnName("Model");
            this.HasRequired(c => c.Owner).WithMany(p => p.Cars).HasForeignKey(c => c.OwnerId);
        }
    }
}
