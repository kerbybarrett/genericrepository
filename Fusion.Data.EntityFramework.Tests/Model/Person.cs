﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Fusion.Data.EntityFramework.Tests
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Car> Cars { get; set; }

        public Person() 
        {
            Cars = new Collection<Car>();
        }
    }
}
