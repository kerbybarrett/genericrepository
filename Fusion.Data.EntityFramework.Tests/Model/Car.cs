﻿namespace Fusion.Data.EntityFramework.Tests
{
    public class Car
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public int OwnerId { get; set; }
        public virtual Person Owner { get; set; }
    }
}
