﻿namespace Fusion.Data.EntityFramework.Tests
{
    /// <summary>
    /// Convenience wrapper for instantiating a new repository with a given context
    /// </summary>
    public class ModelRepository : Repository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModelRepository"/> class.
        /// </summary>
        public ModelRepository() : base(new ModelContext())
        {
        }
    }
}
