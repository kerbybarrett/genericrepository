﻿using System.Data.Entity;

namespace Fusion.Data.EntityFramework.Tests
{
    public class ModelContextInitializer : DropCreateDatabaseAlways<ModelContext>
    {
        /// <summary>
        /// Seeds the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        protected override void Seed(ModelContext context)
        {
            var person = new Person { Name = "Test User" };

            context.People.Add(person);
            context.Cars.Add(new Car { Model = "Fiat", Owner = person });

            context.SaveChanges();
        }
    }
}
