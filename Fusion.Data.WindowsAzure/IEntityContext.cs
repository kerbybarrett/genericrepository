﻿using System;

namespace Fusion.Data.WindowsAzure
{
    /// <summary>
    /// Contract for an Azure table storage entity context
    /// </summary>
    public interface IEntityContext : IDisposable
    {
        //IDbSet<TEntity> Set<TEntity>() where TEntity : class;
    }
}
