﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fusion.Data.WindowsAzure
{
    /// <summary>
    /// Concrete Unit of Work for Azure table storage
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        public bool IsInTransaction
        {
            get { throw new NotImplementedException(); }
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            throw new NotImplementedException();
        }

        public void RollBack()
        {
            throw new NotImplementedException();
        }

        public bool Commit()
        {
            throw new NotImplementedException();
        }

        private bool disposed = false;

        /// <summary>
        /// Disposes of the resources associated with the class
        /// </summary>
        /// <param name="disposing"></param>
        public void Dispose(bool disposing)
        {
            if (!disposing || disposed)
                return;



            disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
