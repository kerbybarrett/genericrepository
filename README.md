# Generic Repository

There are a multitude of repository / unit of work implementations out there.  I created one a couple of years ago
based on a codeplex.com example I saw at the time, but there were some things I didn't like about it and I decided to update
the implementation.

##Credits

Most credit goes to Tugberk Ugurlu as his approach was very consistent with my previous implementation and formed the foundation for my updated implementation.  
http://www.tugberkugurlu.com/archive/clean-better-and-sexier-generic-repository-implementation-for-entity-framework   
https://github.com/tugberkugurlu/GenericRepository  

Other credit goes to the following projects for input into the final solution:  

https://genericunitofworkandrepositories.codeplex.com/  
https://github.com/squareconnection/AzureTSProvider  

# Approach and Differences

There were a handful of things about existing implentations that I felt were shortcomings or not supported.  These were:

- Keep the implementation simple (more complex requirements should be supported through the existing simple interface(s))
- Support IDisposable everywhere it made sense
- Not predicated on any specific application lifecycle or dependency injection / IOC framework (that specification should be determined externally)
- Not require models to inherit from an interface (models are 100% repository-agnostic)
- Support Unit of Work pattern across repositories

# Roadmap

The following are items that I would like to continue working on to make the implementation "better":

- Support for DiffGraph (https://github.com/refactorthis/GraphDiff)
- Use LinkKit or equivalent to build optimized composite predicates
- Add Azure TableStore implementation and unit tests
- Support Unit of Work more cleanly across repositories; today it can be done by sharing contexts across repositories, but it has constraints
- Samples and guidance?