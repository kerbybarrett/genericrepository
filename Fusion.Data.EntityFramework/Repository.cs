﻿using System;
using System.Data.Entity;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

namespace Fusion.Data.EntityFramework
{
    // TODO: Consider creating predicate builder using LINQKit to do dynamic Expression<Func<TEntity, bool>> combination

    /// <summary>
    /// Concrete Repository implementation leveraging an Entity Framework context as the backing store
    /// </summary>
    public class Repository : IRepository, IDisposable
    {
        private readonly EntityContext context;
        private IUnitOfWork unitOfWork;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository" /> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public Repository(EntityContext context)
        {
            Contract.Requires<ArgumentNullException>(context != null);

            this.context = context;
        }

        /// <summary>
        /// Adds the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        public void Add<TEntity>(TEntity entity) where TEntity : class
        {
            context.SetAsAdded(entity);
        }

        // TODO: Investigate use of GraphDiff for managing deltas for entire object graphs : https://github.com/refactorthis/GraphDiff

        /// <summary>
        /// Adds the graph.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        public void AddGraph<TEntity>(TEntity entity) where TEntity : class
        {
            context.Set<TEntity>().Add(entity);
            context.SetAsAdded(entity);
        }

        // TODO: Investigate use of GraphDiff for managing deltas for entire object graphs : https://github.com/refactorthis/GraphDiff

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        public void Update<TEntity>(TEntity entity) where TEntity : class
        {
            context.SetAsModified(entity);
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="entity">The entity.</param>
        public void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            context.SetAsDeleted(entity);
        }

        /// <summary>
        /// Gets the query.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <returns></returns>
        public IQueryable<TEntity> GetQuery<TEntity>() where TEntity : class
        {
            return context.Set<TEntity>();
        }

        /// <summary>
        /// Gets the query.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="predicate">The predicate.</param>
        /// <returns></returns>
        public IQueryable<TEntity> GetQuery<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return GetQuery<TEntity>().Where(predicate);
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns>
        /// The number of records that were affected
        /// </returns>
        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        /// <summary>
        /// Gets the unit of work.
        /// </summary>
        /// <value>
        /// The unit of work.
        /// </value>
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return unitOfWork ?? (unitOfWork = new UnitOfWork(context));
            }
        }

        /// <summary>
        /// Gets the query.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<TEntity> GetQueryWithGraph<TEntity>(params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : class
        {
            var queryable = GetQuery<TEntity>();
            return includeProperties.Aggregate(queryable, (current, includeProperty) => current.Include(includeProperty));
        }

        /// <summary>
        /// Gets the query.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        public IQueryable<TEntity> GetQueryWithGraph<TEntity>(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : class
        {
            var queryable = GetQuery(predicate);
            return includeProperties.Aggregate(queryable, (current, includeProperty) => current.Include(includeProperty));
        }

        private bool disposed = false;

        /// <summary>
        /// Disposes of the resources associated with the class
        /// </summary>
        /// <param name="disposing"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Dispose(bool disposing)
        {
            if (!disposing || disposed)
                return;

            if (unitOfWork != null)
            {
                unitOfWork.Dispose();
            }

            if (context != null)
            {
                context.Dispose();
            }

            disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
