﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity;

namespace Fusion.Data.EntityFramework
{
    /// <summary>
    /// Concrete implementation for Unit of Work pattern
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext context;
        private DbContextTransaction transaction;

        /// <summary>
        /// Gets whether the operation is inside of a transaction.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool IsInTransaction
        {
            get { return transaction != null; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UnitOfWork(DbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <returns></returns>
        public int SaveChanges()
        {
            if (IsInTransaction)
            {
                throw new InvalidOperationException("A transaction is running. Call Commit instead.");
            }

            return context.SaveChanges();
        }

        /// <summary>
        /// Begins the transaction.
        /// </summary>
        /// <param name="isolationLevel">The isolation level.</param>
        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            if (transaction != null)
            {
                throw new InvalidOperationException("Cannot begin a new transaction while an existing transaction is still running. " +
                                                "Please commit or rollback the existing transaction before starting a new one.");
            }

            OpenConnection();
            transaction = context.Database.BeginTransaction(isolationLevel);
        }

        /// <summary>
        /// Rolls back a transaction.
        /// </summary>
        public void RollBack()
        {
            if (transaction == null)
            {
                throw new InvalidOperationException("Cannot roll back a transaction while there is no transaction running.");
            }

            transaction.Rollback();
            ReleaseCurrentTransaction();
        }

        /// <summary>
        /// Commits the transaction.
        /// </summary>
        /// <returns></returns>
        public bool Commit()
        {
            if (transaction == null)
            {
                throw new InvalidOperationException("Cannot commit a transaction while there is no transaction running.");
            }

            try
            {
                context.SaveChanges();
                transaction.Commit();
                ReleaseCurrentTransaction();
                return true;
            }
            catch
            {
                RollBack();
                throw;
            }
        }

        private bool disposed = false;

        /// <summary>
        /// Disposes of the resources associated with the class
        /// </summary>
        /// <param name="disposing"></param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Dispose(bool disposing)
        {
            if (!disposing || disposed)
                return;

            if (IsInTransaction) // Needs a rollback
            {
                RollBack();
            }

            disposed = true;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void OpenConnection()
        {
            if (context.Database.Connection.State == ConnectionState.Closed)
                context.Database.Connection.Open();
        }

        /// <summary>
        /// Releases the current transaction
        /// </summary>
        protected void ReleaseCurrentTransaction()
        {
            if (transaction != null)
            {
                transaction.Dispose();
                transaction = null;
            }
        }
    }
}
