﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Fusion.Data.EntityFramework
{
    /// <summary>
    /// Contract for interacting with a repository specific to Entity Framework
    /// </summary>
    public interface IRepository : Data.IRepository
    {
        /// <summary>
        /// Returns an <see cref="IQueryable" /> query for the given entity type and including the associated properties
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<TEntity> GetQueryWithGraph<TEntity>(params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : class;

        /// <summary>
        /// Returns an <see cref="IQueryable" /> query for the given entity type meeting a specific predicate and including the associated properties
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="predicate">The predicate.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns></returns>
        IQueryable<TEntity> GetQueryWithGraph<TEntity>(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties) where TEntity : class;
    }
}
