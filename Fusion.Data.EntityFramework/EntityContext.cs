﻿using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace Fusion.Data.EntityFramework
{
    /// <summary>
    /// Abstract implementation of <see cref="IEntityContext"/>
    /// </summary>
    public abstract class EntityContext : DbContext, IEntityContext
    {
        /// <summary>
        /// 
        /// </summary>
        protected EntityContext() { 
        }

        /// <summary>
        /// Constructs a new context instance
        /// </summary>
        /// <param name="model"></param>
        protected EntityContext(DbCompiledModel model) : base(model) { 
        }

        /// <summary>
        /// Constructs a new context instance
        /// </summary>
        /// <param name="nameOrConnectionString"></param>
        protected EntityContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        { 
        }

        /// <summary>
        /// Constructs a new context instance
        /// </summary>
        /// <param name="existingConnection"></param>
        /// <param name="contextOwnsConnection"></param>
        protected EntityContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        { 
        }   

        /// <summary>
        /// Constructs a new context instance
        /// </summary>
        /// <param name="objectContext"></param>
        /// <param name="entitiesContextOwnsObjectContext"></param>
        protected EntityContext(ObjectContext objectContext, bool entitiesContextOwnsObjectContext)
            : base(objectContext, entitiesContextOwnsObjectContext) { 
        }

        /// <summary>
        /// Constructs a new context instance
        /// </summary>
        /// <param name="nameOrConnectionString"></param>
        /// <param name="model"></param>
        protected EntityContext(string nameOrConnectionString, DbCompiledModel model) 
            : base(nameOrConnectionString, model) { 
        }

        /// <summary>
        /// Constructs a new context instance
        /// </summary>
        /// <param name="existingConnection"></param>
        /// <param name="model"></param>
        /// <param name="contextOwnsConnection"></param>
        protected EntityContext(DbConnection existingConnection, DbCompiledModel model, bool contextOwnsConnection) 
            : base(existingConnection, model, contextOwnsConnection) { 
        }

        /// <summary>
        /// Returns an IDbSet instance for access to entities of the given type in the context, the ObjectStateManager, and the underlying store.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        /// <summary>
        /// Sets the entity to an added state
        /// </summary>
        /// <param name="entity"></param>
        public void SetAsAdded<TEntity>(TEntity entity) where TEntity : class
        {
            var dbEntityEntry = GetDbEntityEntryInternal(entity);
            dbEntityEntry.State = EntityState.Added;
        }

        /// <summary>
        /// Sets the entity to a modified state
        /// </summary>
        /// <param name="entity"></param>
        public void SetAsModified<TEntity>(TEntity entity) where TEntity : class
        {
            var dbEntityEntry = GetDbEntityEntryInternal(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        /// <summary>
        /// Sets the entity to a deleted state
        /// </summary>
        /// <param name="entity"></param>
        public void SetAsDeleted<TEntity>(TEntity entity) where TEntity : class
        {
            var dbEntityEntry = GetDbEntityEntryInternal(entity);
            dbEntityEntry.State = EntityState.Deleted;
        }

        /// <summary>
        /// Internal method for getting a DbSet object
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected DbEntityEntry<TEntity> GetDbEntityEntryInternal<TEntity>(TEntity entity) where TEntity : class
        {
            var dbEntityEntry = base.Entry(entity);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                base.Set<TEntity>().Attach(entity);
            }

            return dbEntityEntry;
        }
    }
}
