﻿using System;
using System.Data.Entity;

namespace Fusion.Data.EntityFramework
{
    /// <summary>
    /// Contract for an Entity Framework entity context
    /// </summary>
    public interface IEntityContext : IDisposable
    {
        /// <summary>
        /// Returns an IDbSet instance for access to entities of the given type in the context, the ObjectStateManager, and the underlying store. 
        /// </summary>
        IDbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Sets the entity to an added state
        /// </summary>
        /// <param name="entity"></param>
        void SetAsAdded<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Sets the entity to a modified state
        /// </summary>
        /// <param name="entity"></param>
        void SetAsModified<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Sets the entity to a deleted state
        /// </summary>
        /// <param name="entity"></param>
        void SetAsDeleted<TEntity>(TEntity entity) where TEntity : class;

        /// <summary>
        /// Saves all changes made in this context to the underlying database.
        /// </summary>
        /// <returns></returns>
        int SaveChanges();
    }
}
