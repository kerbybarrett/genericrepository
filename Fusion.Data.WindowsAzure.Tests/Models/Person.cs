﻿using System.Globalization;
using Microsoft.WindowsAzure.Storage.Table;

namespace Fusion.Data.WindowsAzure.Tests.Models
{
    public class Person : TableEntity
    {
        public int Id { get; private set; }

        public Person(int id)
        {
            PartitionKey = "UnitTestPartition";
            RowKey = id.ToString(CultureInfo.InvariantCulture);
            Id = id;
        }

        public string Name { get; set; }
    }
}
