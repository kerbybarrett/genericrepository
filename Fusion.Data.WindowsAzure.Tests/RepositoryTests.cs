﻿using Fusion.Data.WindowsAzure.Tests.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Fusion.Data.WindowsAzure.Tests
{
    [TestClass]
    public class RepositoryTests
    {
        [TestMethod]
        public void Raw_AzureStorage_Test()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("ModelEntitiesConnectionString"));

            // Create the table client.
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            // Create the table if it doesn't exist.
            CloudTable table = tableClient.GetTableReference("UnitTest1");
            table.CreateIfNotExists();

            var retrieveOperation = TableOperation.Retrieve<Person>("UnitTestPartition", "1");
            var result = table.Execute(retrieveOperation);

            var item = result.Result as Person;

            if (item != null)
            {
                table.Execute(TableOperation.Delete(item));
            }

            var person = new Person(1);
            table.Execute(TableOperation.Insert(person));
        }
    }
}
